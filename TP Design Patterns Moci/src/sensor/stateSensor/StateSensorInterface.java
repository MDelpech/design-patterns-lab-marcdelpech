package sensor.stateSensor;

import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public interface StateSensorInterface {
    public boolean getStatus();
    
    public void update() throws SensorNotActivatedException;
    
    public double getValue() throws SensorNotActivatedException;
}
