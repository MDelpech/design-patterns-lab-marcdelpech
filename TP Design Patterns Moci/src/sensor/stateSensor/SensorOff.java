package sensor.stateSensor;

import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public class SensorOff implements StateSensorInterface {

    @Override
    public boolean getStatus() {
        return false;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        throw new SensorNotActivatedException("Sensor must be activated to get its value.");
    }
    
}
