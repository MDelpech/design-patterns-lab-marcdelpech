package sensor.stateSensor;

import java.util.Random;
import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public class SensorOn implements StateSensorInterface {
    double value = 0;
    
    @Override
    public boolean getStatus() {
        return true;
    }

    @Override
    public void update() throws SensorNotActivatedException {
        value = (new Random()).nextDouble() * 100;
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        return value;
    }
}
