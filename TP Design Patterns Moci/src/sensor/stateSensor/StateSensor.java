package sensor.stateSensor;

import sensor.AbstractSensor;
import sensor.ISensor;
import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public class StateSensor extends AbstractSensor implements ISensor {
    StateSensorInterface sensor;
    
    public void on() {
        sensor = new SensorOn();
    }

    public void off() {
        sensor = new SensorOff();
    }

    public boolean getStatus() {
        return sensor.getStatus();
    }

    public void update() throws SensorNotActivatedException {
        sensor.update();
        this.notifyAO();
    }

    public double getValue() throws SensorNotActivatedException {
        return sensor.getValue();
    }
    
}
