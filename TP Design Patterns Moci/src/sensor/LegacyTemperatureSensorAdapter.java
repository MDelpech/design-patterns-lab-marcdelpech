package sensor;

/**
 *
 * @author Marc
 */
public class LegacyTemperatureSensorAdapter implements ISensor {
    
    private LegacyTemperatureSensor lts;
    private double temp = 0;

    public LegacyTemperatureSensorAdapter() {
        lts = new LegacyTemperatureSensor();
    }
    
    public void on() {
        if (!lts.getStatus()) {
            lts.onOff();
        } else {
            System.out.println("Sensor is already active");
        }
    }
    
    public void off() {
        if (lts.getStatus()) {
            lts.onOff();
        } else {
            System.out.println("Sesor is already off");
        }
    }

    public boolean getStatus() {
        return lts.getStatus();
    }
    
    public void update() throws SensorNotActivatedException {
        if (this.getStatus()) {
            temp = lts.getTemperature();
        } else {
            throw new SensorNotActivatedException("Sensor must be activated before acquiring new values.");
        }
    }
    
    public double getValue() throws SensorNotActivatedException {
        if (this.getStatus()) {
            return temp;
        } else {
            throw new SensorNotActivatedException("Sensor must be activated to get its value.");
        }
    }
    
}
