package sensor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Marc
 */
public class ProxySensor implements ISensor{
    private ISensor sensor;
    private DateFormat date;
    private Calendar calendar;
    
    public ProxySensor(ISensor sensor) {
        this.sensor = sensor;
        date = new SimpleDateFormat("dd/MM/yyy HH:mm");
        calendar = Calendar.getInstance();
    }
    
    @Override
    public void on() {
        System.out.println(date.format(calendar.getTime()) + " Sensor turned on");
        sensor.on();
    }

    @Override
    public void off() {
        System.out.println(date.format(calendar.getTime()) + " Sensor turned off");
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        System.out.println(date.format(calendar.getTime()) + " Sensor status :");
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        System.out.println(date.format(calendar.getTime()) + " Sensor updated");
        sensor.update();
    }

    @Override
    public double getValue() throws SensorNotActivatedException {
        System.out.println(date.format(calendar.getTime()) + " Sensor value : ");
        return sensor.getValue();
    }
    
}
