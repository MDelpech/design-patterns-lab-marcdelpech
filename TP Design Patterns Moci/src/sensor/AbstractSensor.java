package sensor;

import ui.AbstractObserver;
import java.util.ArrayList;

/**
 * Classe correspondant à l'observable
 * @author Marc
 */
public abstract class AbstractSensor implements ISensor {
    private ArrayList<AbstractObserver> aoList;
    
    public AbstractSensor() {
        this.aoList = new ArrayList<AbstractObserver>();
    }
    
    public void attach(AbstractObserver ao) {
        aoList.add(ao);
    }
    
    public void detach(AbstractObserver ao) {
        if (aoList.contains(ao)) {
            aoList.remove(ao);
        }
    }
    
    public void notifyAO() throws SensorNotActivatedException {
        for(int i = 0 ; i < aoList.size() ; i++) {
            aoList.get(i).update();
        }
    }

    public abstract void on();
    
    public abstract void off();

    public abstract boolean getStatus();

    public abstract void update() throws SensorNotActivatedException;

    public abstract double getValue() throws SensorNotActivatedException;   
    
}
