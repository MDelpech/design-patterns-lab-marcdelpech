package sensor.commandSensor;

import sensor.AbstractSensor;

/**
 *
 * @author Marc
 */
public class CommandOff extends AbstractCommandSensor {

    public CommandOff(AbstractSensor sensor) {
        super(sensor);
    }
    
    public void exec() {
        sensor.off();
    }
}
