package sensor.commandSensor;

import sensor.AbstractSensor;
import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public class CommandUpdate extends AbstractCommandSensor {

    public CommandUpdate(AbstractSensor sensor) {
        super(sensor);
    }

    public void exec() {
        try {
            sensor.update();
        } catch (SensorNotActivatedException sensorNotActivatedException) {
            sensorNotActivatedException.printStackTrace();
        }
    }
}
