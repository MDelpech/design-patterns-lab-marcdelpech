package sensor.commandSensor;

import sensor.AbstractSensor;

/**
 *
 * @author Marc
 */
public abstract class AbstractCommandSensor {
    protected AbstractSensor sensor;
    
    public AbstractCommandSensor(AbstractSensor sensor) {
        this.sensor = sensor;
    }
    
    public abstract void exec();
}
