package sensor.commandSensor;

import sensor.AbstractSensor;

/**
 *
 * @author Marc
 */
public class CommandOn extends AbstractCommandSensor {

    public CommandOn(AbstractSensor sensor) {
        super(sensor);
    }
    
    public void exec() {
        sensor.on();
    }
}
