package sensor.decoratorSensor;

import sensor.AbstractSensor;
import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public abstract class AbstractDecorator extends AbstractSensor {
    protected AbstractSensor sensor;
    
    public AbstractDecorator(AbstractSensor sensor) {
        this.sensor = sensor;
    }

    @Override
    public void on() {
        sensor.on();
    }

    @Override
    public void off() {
        sensor.off();
    }

    @Override
    public boolean getStatus() {
        return sensor.getStatus();
    }

    @Override
    public void update() throws SensorNotActivatedException {
        sensor.update();
    }

    @Override
    public abstract double getValue() throws SensorNotActivatedException;
    // on ne défini pas cette méthode, car à l'inverse des 4 autres, elle n'est pas commune à toutes les sous-classes
}
