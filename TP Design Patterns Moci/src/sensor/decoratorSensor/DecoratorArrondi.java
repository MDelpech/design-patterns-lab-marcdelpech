package sensor.decoratorSensor;

import sensor.AbstractSensor;
import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public class DecoratorArrondi extends AbstractDecorator {
    
    public DecoratorArrondi(AbstractSensor sensor) {
        super(sensor);
    }
    
    public double getValue() throws SensorNotActivatedException {
        return Math.rint(sensor.getValue()); // on renvoie la valeur entière la plus proche de celle du capteur
    }
}
