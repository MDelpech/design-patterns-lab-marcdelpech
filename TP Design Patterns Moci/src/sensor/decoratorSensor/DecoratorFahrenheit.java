package sensor.decoratorSensor;

import sensor.AbstractSensor;
import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public class DecoratorFahrenheit extends AbstractDecorator {
    public DecoratorFahrenheit(AbstractSensor sensor) {
        super(sensor);
    }
    
    public double getValue() throws SensorNotActivatedException {
        return sensor.getValue()*1.8 +32;
    }
}
