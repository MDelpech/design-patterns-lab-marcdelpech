import sensor.AbstractSensor;
import sensor.stateSensor.StateSensor;
import ui.MainWindow;

/**
 *
 * @author Marc
 */
public class StateSwingApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        AbstractSensor sensor = new StateSensor();
        new MainWindow(sensor);
    }
    
}
