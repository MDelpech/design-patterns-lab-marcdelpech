import sensor.AbstractSensor;
import sensor.TemperatureSensor;
import ui.MainWindow;

public class SwingApp {

    public static void main(String[] args) {
        AbstractSensor sensor = new TemperatureSensor();
        new MainWindow(sensor);
    }

}
