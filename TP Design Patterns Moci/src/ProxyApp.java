import sensor.ISensor;
import sensor.ProxySensor;
import sensor.TemperatureSensor;
import ui.ConsoleUI;

public class ProxyApp {

    public static void main(String[] args) {
        ISensor tempSensor = new TemperatureSensor();
        ISensor sensor = new ProxySensor(tempSensor);
        new ConsoleUI(sensor);
    }

}
