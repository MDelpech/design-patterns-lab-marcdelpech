import sensor.ISensor;
import sensor.LegacyTemperatureSensorAdapter;
import ui.ConsoleUI;

/**
 *
 * @author Marc
 */
public class AdapterApp {

    public static void main(String[] args) {
        ISensor sensor = new LegacyTemperatureSensorAdapter();
        new ConsoleUI(sensor);
    }

    
}
