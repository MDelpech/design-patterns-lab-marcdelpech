package ui;

import sensor.SensorNotActivatedException;

/**
 *
 * @author Marc
 */
public interface AbstractObserver {
    public void update();
}
