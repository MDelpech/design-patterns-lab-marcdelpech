package ui;

import sensor.ISensor;
import sensor.SensorNotActivatedException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import sensor.AbstractSensor;
import sensor.commandSensor.CommandOff;
import sensor.commandSensor.CommandOn;
import sensor.commandSensor.CommandUpdate;

public class SensorView extends JPanel implements AbstractObserver {
    private AbstractSensor sensor;
    private CommandOff cOff;
    private CommandOn cOn;
    private CommandUpdate cUpdate;
    
    private JLabel value = new JLabel("N/A °C");
    private JButton on = new JButton("On");
    private JButton off = new JButton("Off");
    private JButton update = new JButton("Acquire");

    public SensorView(AbstractSensor c) {
        this.sensor = c;
        sensor.attach(this);
        cOff = new CommandOff(sensor);
        cOn = new CommandOn(sensor);
        cUpdate = new CommandUpdate(sensor);
        this.setLayout(new BorderLayout());

        value.setHorizontalAlignment(SwingConstants.CENTER);
        Font sensorValueFont = new Font("Sans Serif", Font.BOLD, 18);
        value.setFont(sensorValueFont);

        this.add(value, BorderLayout.CENTER);


        on.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cOn.exec();
            }
        });

        off.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cOff.exec();
            }
        });

        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cUpdate.exec();
            }
        });

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(1, 3));
        buttonsPanel.add(update);
        buttonsPanel.add(on);
        buttonsPanel.add(off);

        this.add(buttonsPanel, BorderLayout.SOUTH);
    }

    public void update() {
        try {
            Double value = sensor.getValue();
            this.value.setText(value.toString() + " °C");
        } catch (SensorNotActivatedException e) {
            e.printStackTrace();
        }
    }
}
